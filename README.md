## Local Library

MDN's "Local Library" Express (Node) [tutorial](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Tutorial_local_library_website), in which we develop a website that might be used to manage the catalog for a local library.

After completing the Local Library tutorial, I wanted to go back over the material and make a second version with my new understanding of how Node/Express/Mongodb worked.

Due to my basic understanding of github at the time, I needed to rebuild the github repository from a local version.  When I rebuilt this repo, I used an Express boilerplate repo I created for future projects in an endeavour to learn best practices on app structure. 

### Node.js | Express | Mongodb

- Explored how to set up a server with node.
- Basic structure of an app's file tree.
- Using npm packages and view engines.
- Connecting to and interacting with Mongodb.
